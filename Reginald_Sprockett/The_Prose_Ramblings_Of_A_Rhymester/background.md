# Background on The Prose Ramblings Of A Rhymester

Author: Reginald Sprockett

Title: The Prose Ramblings Of A Rhymester

# Synopsis

A collection of whymsical essays.

# Literal quotes

# Trivia

The book is bound in limp purple leather.

Reginald Sprockett is a friend of Vanessa Cook, and has given her a
signed copy.

# Wodehouse origin

Aunts Aren't Gentlemen
